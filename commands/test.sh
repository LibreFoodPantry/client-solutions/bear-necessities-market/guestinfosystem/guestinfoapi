#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "$SCRIPT_DIR/.." || exit

docker run --rm -v "${PWD}/specification":/specification \
    -w /specification jeanberu/swagger-cli:4.0.4 \
    swagger-cli validate index.yaml
