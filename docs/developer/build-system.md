# Build System

The build system is responsible for building the products
of this project, which is a bundled copy of the openapi
specification.

```bash
commands/build.sh
```

This runs `commands/build.sh` which generates `build/openapi.yaml` using [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).